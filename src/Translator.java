
public class Translator {
	public String phrase, punctuation="",tPhrase="";
	public static String NIL="nil";
	public Translator(String inputPhrase) {
		phrase= inputPhrase;
	}
	
	public String getPhrase() {
		
		return phrase;
	}
	
	public String translate() {
		
		if(phrase.isEmpty()) {
			return NIL;
		}
		
		if(phrase.contains(" ")) {
			String words[]= phrase.split(" ");
			int countWords=words.length;
			String fphrase="";
			Translator translator;
			for(int i=0;i<countWords-1;i++) {
				translator= new Translator(words[i]);
				fphrase= fphrase+translator.translate()+" ";
			}
			translator= new Translator(words[countWords-1]);
			fphrase= fphrase+translator.translate();
			return fphrase;
		}
		if(phrase.contains("-")) {
			String words[]= phrase.split("-");
			int countWords=words.length;
			String fphrase="";
			Translator translator;
			for(int i=0;i<countWords-1;i++) {
				translator= new Translator(words[i]);
				fphrase= fphrase+translator.translate()+"-";
			}
			translator= new Translator(words[countWords-1]);
			fphrase= fphrase+translator.translate();
			return fphrase;
		}
		
		if(vowelStart()) {
			if(phrase.endsWith("y")){
				if(checkPunctuation()){
					punctuation= phrase.charAt(phrase.length()-1)+"";
				}	
				return phrase+"nay"+punctuation;
			}
			
			if(vowelEnd()) {
				if(checkPunctuation()){
					punctuation= phrase.charAt(phrase.length()-1)+"";
				}	
				return phrase+"yay"+punctuation;
			}
			
			if(!vowelEnd()) {
				if(checkPunctuation()){
					punctuation= phrase.charAt(phrase.length()-1)+"";
				}	
				return phrase+"ay"+punctuation;
			}
		}
		else {
			if(!vowelStart()) {
				
				if(checkVowel(1)) {	
					if(checkPunctuation()){
						punctuation= phrase.charAt(phrase.length()-1)+"";
						phrase= phrase.substring(0,phrase.length()-1);
					}
					char[] phrase2= phrase.toCharArray();
					
					char temp= phrase2[0];
					String fphrase= phrase.substring(1)+temp+"ay"+punctuation;
					return fphrase;
				}
				else {
					int consonant=2;
					while(checkVowel(3)==false) {
						consonant++;
					}
					String fphrase= phrase.substring(consonant-1)+phrase.substring(0,consonant-1)+"ay"+punctuation;
					return fphrase;
				}
			}
		}
		
		
		return NIL;

	}
	
	public Boolean vowelStart(){
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u");
	}
	
	public Boolean vowelEnd(){
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u");
	}
	public Boolean checkVowel(int i){
		return phrase.startsWith("a",i) ||  phrase.startsWith("e",i) || phrase.startsWith("i",i) || phrase.startsWith("o",i) || phrase.startsWith("u",i) ||  false;
	}
	
	private boolean checkPunctuation() {
		return phrase.endsWith(".") || phrase.endsWith(",") || phrase.endsWith(";") || phrase.endsWith(":") ||
				 phrase.endsWith("?") ||  phrase.endsWith("!") || phrase.endsWith("'") ||
				 phrase.endsWith("(") || phrase.endsWith(")");
	}
	


	
}
