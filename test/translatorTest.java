import static org.junit.Assert.*;

import org.junit.Test;

public class translatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase= "hello world";
		Translator translator= new Translator(inputPhrase);
		assertEquals("hello world",translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase="";
		Translator translator= new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowalEndingWithY() {
		String inputPhrase="any";
		Translator translator= new Translator(inputPhrase);

		assertEquals("anynay", translator.translate());
		}
	
	@Test
	public void testTranslationPhraseStartingWithVowaleEndingVowal() {
		String inputPhrase="anno";
		Translator translator= new Translator(inputPhrase);

		assertEquals("annoyay", translator.translate());
		}
	
	@Test
	public void testTranslationEndingConsonant() {
		String inputPhrase="ask";
		Translator translator= new Translator(inputPhrase);

		assertEquals("askay", translator.translate());
		}
	@Test
	public void testTranslationPhraseStartingWithConsonant() {
		String inputPhrase="hello";
		Translator translator= new Translator(inputPhrase);

		assertEquals("ellohay", translator.translate());
		}
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() {
		String inputPhrase="know";
		Translator translator= new Translator(inputPhrase);

		assertEquals("ownkay", translator.translate());
		}
	
	@Test
	public void testTranslatorMultipleWordsWithSpace() {
		String inputPhrase="hello world";
		Translator translator=new Translator(inputPhrase);
		
		assertEquals("ellohay orldway",translator.translate());
	}
	
	@Test
	public void testTranslatorMultipleWordsWithScore() {
		String inputPhrase="well-being";
		Translator translator=new Translator(inputPhrase);
		
		assertEquals("ellway-eingbay",translator.translate());
	}
	
	@Test
	public void testTranslationWithPunctuation() {
		String inputPhrase="hello world!";
		Translator translator=new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!",translator.translate());
	}
}
